import moment from "moment";

const partOfDay: () => string = () => {
  const [month, hour] = moment().format("M, kk").split(",").map(el => parseInt(el));
  if (month >= 3 && month <= 8) {
    if (hour >= 5 && hour < 12) {
      return "morning";
    }
    else if (hour >= 12 && hour < 17) {
      return "afternoon";
    }
    else if (hour >= 17 && hour < 20) {
      return "evening";
    }
    else {
      return "night"
    }
  }
  else {
    if (hour >= 5 && hour < 12) {
      return "morning";
    }
    else if (hour >= 12 && hour < 15) {
      return "afternoon";
    }
    else if (hour >= 15 && hour < 17) {
      return "evening";
    }
    else {
      return "night"
    }
  }
}

export default partOfDay;