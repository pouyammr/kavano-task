import actionTypes from "./actionTypes";
import moment from 'moment';
import {v4 as uuidv4} from 'uuid';
import request from 'pages/api/request';

moment.defaultFormat = "YYYY/MM/DD";

const newTask = (title: string, date: string) => {
  return {
    type: actionTypes.newTask,
    payload: {
      taskTitle: title,
      taskDate: moment(date, moment.defaultFormat).valueOf(),
      taskID: uuidv4(),
    }
  }
}

const taskCompleted = (title: string) => {
  return {
    type: actionTypes.taskCompleted,
    taskTitle: title,
  }
}

const removeCompletedTask = (title: string) => {
  return {
    type: actionTypes.removeCompletedTask,
    taskTitle: title
  }
}

const setInitialTasks = (payload) => {
  return {
    type: actionTypes.setInitialTasks,
    payload: payload
  }
}

const newTaskSent = () => {
  return {
    type: actionTypes.newTaskSent,
    error: false
  }
}

const completedTaskSent = () => {
  return {
    type: actionTypes.completedTaskSent,
    error: false
  }
}

const removedCompletedTaskSent = () => {
  return {
    type: actionTypes.removedCompletedTaskSent,
    error: false
  }
}

const newTaskFailed = () => {
  return {
    type: actionTypes.newTaskFailed,
    error: true
  }
}

const completedTaskFailed = () => {
  return {
    type: actionTypes.completedTaskFailed,
    error: true
  }
}

const removedCompletedTaskFailed = () => {
  return {
    type: actionTypes.removedCompletedTaskFailed,
    error: true
  }
}

//Thunk functions
const sendNewTask = (tasks) => {
  return async dispatch => {
    const req = await request.put("/tasks.json", tasks);
    req.status === 200 || req.status === 201 ? dispatch(newTaskSent()) : dispatch(newTaskFailed());
    
  }
}

const sendCompletedTask = (state) => {
  return async dispatch => {
    const req = await request.put("/tasks.json", state);
    req.status === 200 || req.status === 201 ? dispatch(completedTaskSent()) : dispatch(completedTaskFailed());
  }
}

const sendRemovedCompletedTask = (state) => {
  return async dispatch => {
    const req = await request.put("/tasks.json", state);
    req.status === 200 || req.status === 201 ? dispatch(removedCompletedTaskSent()) : dispatch(removedCompletedTaskFailed());
  }
}
const getTasks = () => {
  return async dispatch => {
    const res = await request.get("/tasks.json");
    if (res.status === 200) 
    dispatch(setInitialTasks(res.data));
  }
}

export default {
  newTask,
  taskCompleted,
  sendNewTask,
  sendCompletedTask,
  removeCompletedTask,
  sendRemovedCompletedTask,
  getTasks,
  setInitialTasks
}