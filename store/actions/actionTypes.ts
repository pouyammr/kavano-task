const actionTypes = {
  previousMonth: "PREVIOUS_MONTH",
  nextMonth: "NEXT_MONTH",
  setDate: "SET_DATE",
  nextType: "NEXT_TYPE",
  previousType: "PREVIOUS_TYPE",
  setLayout: "SET_LAYOUT",
  newTask: "NEW_TASK",
  taskCompleted: "TASK_COMPLETED",
  removeCompletedTask: "REMOVE_COMPLETED_TASK",
  setInitialTasks: "SET_INITIAL_TASKS",
  getTasks: "GET_TASKS",
  sendNewTask: "SEND_NEW_TASK",
  newTaskSent: "NEW_TASK_SENT",
  newTaskFailed: "NEW_TASK_FAILED",
  sendCompletedTask: "SEND_COMPLETED_TASK",
  completedTaskSent: "COMPLETED_TASK_SENT",
  completedTaskFailed: "COMPLETED_TASK_FAILED",
  sendRemovedCompletedTask: "SEND_REMOVED_COMPLETED_TASK",
  removedCompletedTaskSent: "REMOVED_COMPLETED_TASK_SENT",
  removedCompletedTaskFailed: "REMOVED_COMPLETED_TASK_SENT"
} as const

export default actionTypes;