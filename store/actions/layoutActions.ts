import actionTypes from "./actionTypes"

const previousLayout = () => {
  return {
    type: actionTypes.previousType,
  }
}

const nextLayout = () => {
  return {
    type: actionTypes.nextType,
  }
}

const setLayout = (layout: "DAY" | "WEEK" | "MONTH") => {
  return {
    type: actionTypes.setLayout,
    layout: layout
  }
}

export default { 
  previousLayout,
  nextLayout,
  setLayout 
};