import actionTypes from "./actionTypes";

const nextMonth = () => {
  return {
    type: actionTypes.nextMonth
  }
}

const previousMonth = () => {
  return {
    type: actionTypes.previousMonth
  }
}

const setDate = date => {
  return {
    type: actionTypes.setDate,
    date: date
  }
}
export default {
  nextMonth,
  previousMonth,
  setDate
}
