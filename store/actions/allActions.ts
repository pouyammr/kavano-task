import monthActions from './monthActions';
import taskActions from './taskActions';
import layoutActions from './layoutActions';

const allActions = {
  monthActions,
  taskActions,
  layoutActions
}

export default allActions;