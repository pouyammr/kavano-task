import monthReducer from "./monthReducer";
import layoutReducer from "./layoutReducer";
import taskReducer from "./taskReducer";
import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import { useMemo } from "react";

//Combining reducers
const rootReducer = combineReducers({monthReducer, layoutReducer, taskReducer});

//Defining a initial redux store
let store; 
//Creating a initStore function
const initStore = initialState => createStore(rootReducer, initialState, applyMiddleware(thunk))

export const initializeStore = (preloadedState) => {
  let _store = store ?? initStore(preloadedState)

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState,
    })
    // Reset the current store
    store = undefined
  }

  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') return _store
  // Create the store once in the request
  if (!store) store = _store

  return _store
}

export const useStore = (initialState) => {
  const store = useMemo(() => initializeStore(initialState), [initialState])
  return store
}