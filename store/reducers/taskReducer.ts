import actionTypes from "store/actions/actionTypes";

const initialState = {
  pendingTasks: [], 
  completedTasks: [],
  error: false}
const taskReducer = (state = initialState, action) => {
  switch (action.type) {

    case actionTypes.newTask:
      return {
        ...state,
        pendingTasks: [...state.pendingTasks, action.payload],
      }

    case actionTypes.taskCompleted:
      return {
        ...state,
        completedTasks: [...state.completedTasks, state.pendingTasks.find(el => el.taskTitle === action.taskTitle)],
        pendingTasks: state.pendingTasks.filter(el => el.taskTitle !== action.taskTitle),
      }
    
    case actionTypes.removeCompletedTask:
      return {
        ...state,
        pendingTasks: [...state.pendingTasks, state.completedTasks.find(el => el.taskTitle === action.taskTitle)],
        completedTasks: state.completedTasks.filter(el => el.taskTitle !== action.taskTitle)
      }

  
    case actionTypes.setInitialTasks:
      return {
        ...state,
        ...action.payload,
      }
    
    case actionTypes.newTaskSent:
      return {
        ...state,
        error: action.error
      }
    
    case actionTypes.completedTaskSent:
      return {
        ...state,
        error: action.error
      } 

    case actionTypes.removedCompletedTaskSent:
      return {
        ...state,
        error: action.error
      }

    case actionTypes.newTaskFailed:
      return {
        ...state,
        error: action.error
      }
    
    case actionTypes.completedTaskFailed:
      return {
        ...state,
        error: action.error
      } 

    case actionTypes.removedCompletedTaskFailed:
      return {
        ...state,
        error: action.error
      }

    default:
      return state;
  }
}

export default taskReducer;