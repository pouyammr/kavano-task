import actionTypes from "store/actions/actionTypes";
import moment from "moment";

moment.defaultFormat = "YYYY/MM/DD";

const initialState = moment(new Date, moment.defaultFormat).format();
const monthReducer = (state = initialState, action) => {
  const [year, month, day] = state.split("/").map(el => parseInt(el));
  switch (action.type) {

    case actionTypes.previousMonth: 
      if (month - 1 <= 0) {
        return [year - 1, "12", day].join("/");
      }
      else {
        return [year, month - 1, day].join("/");
      }
    
    case actionTypes.nextMonth:
      if (month + 1 > 12) {
        return [year + 1, "01", day].join("/");
      }
      else {
        return [year, month + 1, day].join("/");
      }
    
    case actionTypes.setDate:
      return moment(action.date).format("YYYY/MM/DD");
      
    default:
      return state;
    }
}

export default monthReducer;