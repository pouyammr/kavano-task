import actionTypes from "store/actions/actionTypes";

const initialState = "MONTH"; 
const layoutReducer = (state = initialState, action) => {
  switch (action.type) {
    
    case actionTypes.previousType:
      switch (state) {
        case "DAY":
          return "MONTH";
        
        case "WEEK":
          return "DAY";
        
        case "MONTH":
          return "WEEK";
        
        default:
          return state;
      }
    
      case actionTypes.nextType:
        switch (state) {
          case "DAY":
            return "WEEK";
          
          case "WEEK":
            return "MONTH";
          
          case "MONTH":
            return "DAY";
          
          default:
            return state;
        }
      
      case actionTypes.setLayout:
        return action.layout;
        
    default:
      return state;
  }
}

export default layoutReducer;