import React from 'react';
import styles from './LayoutChanger.module.scss';
import { useDispatch } from 'react-redux';
import Image from 'next/image';
import cn from 'classnames';
import allActions from 'store/actions/allActions';
import chevronLeft from 'public/assets/chevron-left.svg';
import chevronRight from 'public/assets/chevron-right.svg';

interface LayoutChangerProps {
  layout: "DAY" | "WEEK" | "MONTH"
}

const LayoutChanger: React.FC<LayoutChangerProps> = ({ layout }: { layout: LayoutChangerProps['layout'] }) => {
  const dispatch = useDispatch();

  return (
    <div className={styles['container']}>
      <div className={styles['icon-container']} 
      onClick={() => dispatch(allActions.layoutActions.previousLayout())}>
        <Image src={chevronLeft} width={15} height={20} />
      </div>
      <div className={layout === "DAY" ? cn(styles['layout'],styles['set']) : cn(styles['layout'],styles['unset'])}
        onClick={() => dispatch(allActions.layoutActions.setLayout("DAY"))}>Day</div>
      <div className={layout === "WEEK" ? cn(styles['layout'],styles['set']) : cn(styles['layout'],styles['unset'])}
        onClick={() => dispatch(allActions.layoutActions.setLayout("WEEK"))}>Week</div>
      <div className={layout === "MONTH" ? cn(styles['layout'],styles['set']) : cn(styles['layout'],styles['unset'])}
        onClick={() => dispatch(allActions.layoutActions.setLayout("MONTH"))}>Month</div>
      <div className={styles['icon-container']} 
      onClick={() => dispatch(allActions.layoutActions.nextLayout())}>
        <Image src={chevronRight} width={15} height={20} />
      </div>
    </div>
  )
}

export default LayoutChanger;