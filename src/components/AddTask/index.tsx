import React, { useState } from 'react';
import styles from './AddTask.module.scss';
import Image from 'next/image';
import Modal from 'src/components/Modal';
import Plus from 'public/assets/plus.svg';
import cn from'classnames';

interface AddTaskProps {
  className?: string;
}
const AddTask: React.FC<AddTaskProps> = (props: AddTaskProps) => {
  const [open, setOpen] = useState<boolean>(false);
  const { className } = props;

  return (
    <>
    <div className={cn(styles['container'], className)} onClick={() => setOpen(true)}>
      <div className={styles['icon-container']}>
        <Image src={Plus} width={20} height={20} />
      </div>
      <div className={styles['title']}>Add a Task</div>
    </div>
    {open && <Modal closeModal={() => setOpen(false)} />}
    </>
  )
}

export default AddTask;