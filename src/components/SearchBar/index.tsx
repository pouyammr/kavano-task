import React from 'react';
import styles from './SearchBar.module.scss';
import Image from 'next/image';
import Search from 'public/assets/search.svg';

const SearchBar: React.FC = () => (
  <div className={styles['search-bar']}>
    <div className={styles['search']}>
      <Image src={Search} height={18} width={18} className={styles['search__icon']} />
    </div>
    <input type='text' placeholder='Search' className={styles['search-bar__input']} />
  </div>
)

export default SearchBar;