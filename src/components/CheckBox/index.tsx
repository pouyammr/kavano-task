import React from 'react';
import styles from './CheckBox.module.scss';
import moment from 'moment';
import cn from 'classnames';

interface CheckBoxProps {
  taskTitle: string,
  taskDate: number,
  setLabel: () => void,
  dispatch: any,
  className?: string
}

const CheckBox: React.FC<CheckBoxProps> = (props: CheckBoxProps) => {
  const { taskTitle, taskDate, setLabel, dispatch, className } = props;
  const now = moment();
  let subheader;

  if (now.diff(moment(taskDate), 'days') === -1) {
    subheader = <div className={cn(styles['subheader'], styles['future-task'])}>Tomorrow</div>
  }
  else if (now.diff(moment(taskDate), 'days') === 0) {
    subheader = <div className={cn(styles['subheader'], styles['future-task'])}>Today</div>
  }
  else if (now.diff(moment(taskDate), 'days') > 0) {
    subheader = <div className={cn(styles['subheader'], styles['past-task'])}>{moment(taskDate).fromNow()}</div>
  }
  else {
    subheader = null;
  }
  const handleCheck = () => {
    setLabel();
    dispatch();
  }

  return (
    <div className={cn(styles['check-box'], className)}>
      <div className={styles['button-container']} onClick={() => handleCheck()} >
        <div className={styles['button']} />
      </div>
      <div className={styles['title-container']}>
      <div className={styles['title']}>{taskTitle}</div>
      {subheader ?? null}
      </div>
    </div>
  )
}

export default CheckBox;