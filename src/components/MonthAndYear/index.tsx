import React from 'react';
import styles from './MonthAndYear.module.scss';
import Image from 'next/image';
import { useDispatch } from 'react-redux';
import moment from 'moment';
import allActions from 'store/actions/allActions';
import chevronLeft from 'public/assets/chevron-left.svg';
import chevronRight from 'public/assets/chevron-right.svg';

interface MonthAndYearProps {
  date: string;
}

const MonthAndYear: React.FC<MonthAndYearProps> = ({ date }: { date: MonthAndYearProps['date']}) => {
  const dispatch = useDispatch();
  return (  
    <div className={styles['container']}>
      <div className={styles['icon-container']} 
      onClick={() => dispatch(allActions.monthActions.previousMonth())}>
        <Image src={chevronLeft} width={15} height={20} />
      </div>
      <div className={styles['date']}>
        {moment(date).format('MMMM YYYY')}
      </div>
      <div className={styles['icon-container']} 
      onClick={() => dispatch(allActions.monthActions.nextMonth())}>
        <Image src={chevronRight} width={15} height={20} />
      </div>
    </div>
  )
}

export default MonthAndYear;
