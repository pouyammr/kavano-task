import React, { useEffect, useState } from 'react';
import { useSelector ,useDispatch } from 'react-redux';
import allActions from 'store/actions/allActions';
import styles from './Modal.module.scss';
import Image from 'next/image';
import Close from 'public/assets/close.svg';

interface ModalProps {
  closeModal: () => void;
}
const Modal: React.FC<ModalProps> = ({closeModal}: {closeModal: ModalProps['closeModal']}) => {
  const [date, setDate] = useState<string>("");
  const [task, setTask] = useState<string>("");
  const [submit, setSubmit] = useState<boolean>(false);
  const tasks = useSelector(state => state.taskReducer);

  const dispatch = useDispatch();

  const handleTask = event => {
    setTask(event.target.value);
  }

  const handleDate = event => {
    setDate(event.target.value);
  }

  const handleSubmit = () => {
    dispatch(allActions.taskActions.newTask(task, date))
    if (!tasks.error) {
      setSubmit(true);
      setTimeout(() => closeModal(), 1000);
    }
    else {
      return;
    }
  }

  useEffect(() => {
    dispatch(allActions.taskActions.sendNewTask(tasks))
  }, [submit]);
  
  return (
    <div className={styles['backdrop']}>
      <div className={styles['modal-container']}>
        <div className={styles['close-container']}>
        <Image 
        src={Close} 
        width={25} 
        height={25} 
        className={styles['close']} 
        onClick={closeModal} />
        </div>
        <div className={styles['input-box']}>
          <div className={styles['input-box__header']}>Enter your task title:</div>
          <input 
          type="text" 
          placeholder="Task Title" 
          className={styles['input-box__input']} 
          onChange={event => handleTask(event)} />
        </div>
        <div className={styles['input-box']}>
          <div className={styles['input-box__header']}>Enter your task date:</div>
          <input 
          type="text" 
          placeholder="Task Date" 
          className={styles['input-box__input']} 
          onChange={event => handleDate(event)} />
        </div>
        {tasks.error && <div className={styles['error-message']}>Something has happened. Please try again.</div>}
        <div className={styles['buttons']}>
        <button className={styles['submit']} onClick={() => handleSubmit()}>Submit</button>
        <button className={styles['cancel']} onClick={closeModal}>Cancel</button>
        </div>
      </div>
    </div>
  )
}

export default Modal;