import React, { useState } from 'react';
import styles from './PendingTasks.module.scss';
import AddTask from 'src/components/AddTask';
import CheckBox from 'src/components/CheckBox';
import { useSelector, useDispatch } from 'react-redux';
import allActions from 'store/actions/allActions';
import useDidUpdateEffect from 'hooks/useDidUpdateEffect';
import cn from 'classnames';

type task = {
  taskTitle: string,
  taskDate: number,
  taskID: string,
}

interface PendingTasksProps {
  pendingTasks: task[],
  className?: string
}
const PendingTasks: React.FC<PendingTasksProps> = (props: PendingTasksProps) => {
  const { pendingTasks, className } = props;
  const state = useSelector(state => state.taskReducer);
  const dispatch = useDispatch();
  const [label, setLabel] = useState<boolean>(false);

  useDidUpdateEffect(() => {
    console.log(state);
    dispatch(allActions.taskActions.sendCompletedTask(state));
  },[label]);

  return (
    <div className={cn(styles['container'], className)}>
      <AddTask className={styles['add-task']} />
      <div className={styles['task-container']}>
        {pendingTasks.map(el => <CheckBox 
        taskTitle={el.taskTitle} 
        taskDate={el.taskDate} 
        key={el.taskID} 
        setLabel={() => setLabel(!label)}
        dispatch={() => dispatch(allActions.taskActions.taskCompleted(el.taskTitle))
        } />)}
      </div>
    </div>
  )
}

export default PendingTasks;