import React from 'react';
import styles from './DateDisplay.module.scss';
import moment from 'moment';

interface DateDisplayProps {
  date: string;
}

const DateDisplay: React.FC<DateDisplayProps> = ({ date }: { date: string }) => {
  const now = moment();
  let header;
  if (now.diff(moment(date), 'days') === 1) {
    header = <div className={styles['header']}>Tomorrow</div>;
  }
  else if (now.diff(moment(date), 'days') === 0) {
    header = <div className={styles['header']}>Today</div>;
  }
  else if (now.diff(moment(date), 'days') === -1) {
    header = <div className={styles['header']}>Yesterday</div>;
  }
  else {
    header = null;
  }
  return (
    <div className={styles['container']}>
      {header ?? ''}
      <div className={styles['subheader']}>{moment(date).format("DD, ddd")}</div>
    </div>
  )
}

export default DateDisplay;