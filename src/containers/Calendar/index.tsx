import React from 'react'
import styles from './Calendar.module.scss';
import allActions from 'store/actions/allActions';
import { useDispatch } from 'react-redux';
import moment from 'moment';
import cn from 'classnames';

type task = {
  taskTitle: string,
  taskDate: number,
  taskID: string,
}
interface CalendarProps {
  pendingTasks: task[],
  date: string,
}

const Calendar: React.FC<CalendarProps> = (props: CalendarProps) => {
  const { pendingTasks, date } = props;
  const dispatch = useDispatch();
  const week = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  const startingDayOfMonth = moment(`${moment(date).format("YYYY/MM")}/01`);
  const endingDayOfMonth = moment(`${moment(date).format("YYYY/MM")}/${moment(date).daysInMonth()}`);
  const renderCells = () => {
    const rows = [];
    let days = [];
    let day = startingDayOfMonth;
    let formattedDate;
    let tasks;
    while (day <= endingDayOfMonth) {
      for (let i = 0; i < 7; i++) {
        formattedDate = moment(day).format('D');
        tasks = [];
        const cloneDay = day;
        const cellClass = !moment(day).isSame(startingDayOfMonth, 'month') 
        ? "disabled" 
        : !moment(day).isSame(moment(date), 'day')
          ? "selected"
          : null;
        console.log(day === moment(date));
        days.push(
          <div
          className={cn("col", "cell", cellClass)}
          key={moment(day).format("ddd MMM D YYYY")}
          onClick={() => dispatch(allActions.monthActions.setDate(cloneDay))}>
            <div 
            className={day === moment(date) 
            ? cn(styles['number'], styles['selected-date']) 
            : styles['number']}>{formattedDate}</div>
          {tasks.length !== 0 && 
            tasks.map(el => (
              <div className={styles['task']} key={el.taskID}>{el.taskTitle}</div>
            ))}
          </div>  
        )
        day = moment(day).add(1,'day');
      }
      rows.push(
        <div className={styles['row']} key={moment(day).format("ddd MMM D YYYY")}>
          {days}
        </div>
      );
      days = [];
    }
    return <div className={styles['body']}>{rows}</div>;
  }
  return (
    <div className={styles['container']}>
      <div className={styles['calendar-header']}>
        {week.map((el, index) => (
          <div className={styles['header-title']} key={index}>{el}</div>
        ))}
      </div>
      <div className={styles['calendar']}>
          {renderCells()}
      </div>
    </div>
  )
}

export default Calendar;