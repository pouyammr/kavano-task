import React from 'react';
import styles from './Profile.module.scss';
import Image from 'next/image';
import Nate from 'public/assets/nate.jpg';

import partOfDay from 'helpers/partOfDay';

const Profile: React.FC = () => (
  <div className="row">
    <div className="col-2">
      <Image src={Nate} width={50} height={50} className={styles['profile-photo']} />
    </div>
    <div className="col-10">
      <div className="row">
        <div className={styles['greetings']}>{`Good ${partOfDay()},`}</div>
      </div>
      <div className="row">
        <div className={styles['greetings__name']}>Nate!</div>
      </div>
    </div>
  </div>
)

export default Profile;