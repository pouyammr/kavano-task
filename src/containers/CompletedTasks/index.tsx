import React, { useState } from 'react';
import Image from 'next/image';
import styles from './CompletedTasks.module.scss';
import CheckBox from 'src/components/CheckBox';
import ChevronLeft from 'public/assets/chevron-left.svg';
import useDidUpdateEffect from 'hooks/useDidUpdateEffect';
import { useSelector, useDispatch } from 'react-redux';
import allActions from 'store/actions/allActions';
import cn from 'classnames';

type task = {
  taskTitle: string;
  taskDate: number;
  taskID: string;
}

interface CompletedTasksProps {
  completedTasks: task[]
}

const completedTasks: React.FC<CompletedTasksProps> = (props: CompletedTasksProps) => {
  const { completedTasks } = props;
  const state = useSelector(state => state.taskReducer);
  const [label, setLabel] = useState<boolean>(false);
  const [open, setOpen] = useState<boolean>(false);
  const dispatch = useDispatch();

  useDidUpdateEffect(() => {
    dispatch(allActions.taskActions.sendRemovedCompletedTask(state));
  }, [label])

  return (
    <div className={styles['container']}>
      <div className={!open 
      ? cn(styles['collapsible-header'], styles['header-close'])
      : cn(styles['collapsible-header'], styles['header-open'])} onClick={() => setOpen(!open)}>
        <div className={styles['header-title']}>Completed</div>
        <div className={styles['icon-container']}>
          <Image src={ChevronLeft} width={10} height={15} className={!open ? styles['icon'] : styles['icon__reversed']} />
        </div>
      </div>
      <div className={!open
        ? cn(styles['collapsible-content'], styles['content-close'])
        : cn(styles['collapsible-content'], styles['content-open'])}>
          <div className={styles['task-container']}>
            {completedTasks.map(el => <CheckBox 
            taskTitle={el.taskTitle} 
            taskDate={el.taskDate} 
            key={el.taskID} 
            setLabel={() => setLabel(!label)}
            dispatch={() => dispatch(allActions.taskActions.removeCompletedTask(el.taskTitle))} 
            className={styles['check-box']} />)}
          </div>
      </div>
    </div>
  )
}

export default completedTasks;