import axios from "axios";

const request = axios.create(
    {
        baseURL: "https://kavano-task-default-rtdb.firebaseio.com",
        headers: {"Access-Control-Allow-Origin": "*" }
    }
);

export default request;