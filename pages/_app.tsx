import React from 'react'
import { useStore } from 'store/reducers/store';
import { Provider } from 'react-redux';
import 'styles/globals.scss'
import 'styles/_typography.scss'
import '/public/assets/fonts/css/GigaSans.css';

function MyApp({ Component, pageProps }: { Component: any, pageProps: any}): React.ReactElement {
  const store = useStore(pageProps.initialReduxState);
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  )
}

export default MyApp;