import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import allActions from 'store/actions/allActions';
import cn from 'classnames';
import styles from 'styles/Home.module.scss';
import 'bootstrap/dist/css/bootstrap.css';

import Profile from 'src/containers/Profile';
import SearchBar from 'src/components/SearchBar';
import MonthAndYear from 'src/components/MonthAndYear';
import DateDisplay from 'src/containers/DateDisplay';
import LayoutChanger from 'src/components/LayoutChanger';
import CompletedTasks from 'src/containers/CompletedTasks';
import PendingTasks from 'src/containers/PendingTasks';
import Calendar from 'src/containers/Calendar';

export default function Home(): any {
  const dispatch = useDispatch();
  const state = useSelector(state => state);

  useEffect(() => {
    dispatch(allActions.taskActions.getTasks());
  }, [])
  console.log(state);
  return (
    <div className={cn("container", styles['outer-container'])}>
      <div className={cn("row", styles['inner-container'])}>
        <div className={cn("col-md-4", styles['sidebar'])}>
          <div className={cn("row", styles['profile'])}>
            <Profile />
          </div>
          <div className={cn("row", "justify-content-md-center", styles['tasks'])}>
            <PendingTasks pendingTasks={state.taskReducer.pendingTasks} className={styles['pending-tasks']} />
            <CompletedTasks completedTasks={state.taskReducer.completedTasks} />
          </div>
        </div>
        <div className="col-md-8">
          <div className={cn("row", "align-items-md-center", styles['first-row'])}>
            <MonthAndYear date={state.monthReducer} />
            <DateDisplay date={state.monthReducer} />
            <SearchBar />
          </div>
          <div className={cn("row", "justify-content-md-center", styles['second-row'])}>
            <LayoutChanger layout={state.layoutReducer} />
          </div>
          <div className="row">
            <Calendar pendingTasks={state.taskReducer.PendingTasks} date={state.monthReducer} />
          </div>
        </div>
      </div>
    </div>
  )
}
